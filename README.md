# time-converter

This Shiny app calculates the duration between two timestamps converted into seconds. 

Its use case originated with needing to measure time-on-task from recorded usability session videos.